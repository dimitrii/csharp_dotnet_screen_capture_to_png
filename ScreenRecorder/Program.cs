﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;


namespace ConsoleApplication1
{
     
    public class ScreenRecorder
    {

        static List<Bitmap> images;

        private static string tempDir = Directory.GetCurrentDirectory() + "\\snapshot_" + DateTime.Now.Day + DateTime.Now.Hour + DateTime.Now.Minute + DateTime.Now.Second + "\\";

        private static System.Drawing.Rectangle _Bounds = System.Windows.Forms.Screen.PrimaryScreen.Bounds;
        public static System.Drawing.Rectangle Bounds
        {
            get { return _Bounds; }
            set { _Bounds = value; }
        }

        public static void Record(int count)
        {
            images = new List<Bitmap>(count);
            
            if (!Directory.Exists(tempDir))
                Directory.CreateDirectory(tempDir);

            lock (images)
            {
                int Co = 0;
                do
                {
                    Co += 1;

                    System.Threading.Thread.Sleep(33);

                    System.Drawing.Bitmap X = new System.Drawing.Bitmap(_Bounds.Width, _Bounds.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
                    using (System.Drawing.Graphics G = System.Drawing.Graphics.FromImage(X))
                    {
                        G.CopyFromScreen(_Bounds.Location, new System.Drawing.Point(), _Bounds.Size);
                        System.Drawing.Rectangle CurBounds = new System.Drawing.Rectangle(System.Drawing.Point.Subtract(System.Windows.Forms.Cursor.Position, Bounds.Size), System.Windows.Forms.Cursor.Current.Size);
                        System.Windows.Forms.Cursors.Default.Draw(G, CurBounds);
                    }
                    //images.Add(X);
                    System.IO.FileStream FS = new System.IO.FileStream(tempDir + FormatString(Co.ToString(), 5, '0') + ".png", System.IO.FileMode.OpenOrCreate);
                    X.Save(FS, System.Drawing.Imaging.ImageFormat.Png);
                    X.Dispose();
                    FS.Close();

                } while (Co < count);
            }

        }
        
        public static void CreateFiles() {

            lock (images)
            {
                if (!Directory.Exists(tempDir))
                    Directory.CreateDirectory(tempDir);

                int Co = 0;

                foreach (Bitmap image in images)
                {
                    System.IO.FileStream FS = new System.IO.FileStream(tempDir + FormatString(Co.ToString(), 5, '0') + ".png", System.IO.FileMode.OpenOrCreate);
                    image.Save(FS, System.Drawing.Imaging.ImageFormat.Png);
                    image.Dispose();
                    FS.Close();
                    Co++;
                }
            }
        }

        private static string FormatString(string S, int places, char character)
        {
            if (S.Length >= places)
                return S;
            for (int X = S.Length; X <= places; X++)
            {
                S = character + S;
            }
            return S;
        }

    }

    class Program
    {
        const int DEFAULT_TIME = 3;
        const int MAX_TIME     = 30;

        static void Main(string[] args)
        {
            int length = DEFAULT_TIME;

            if (args.Length > 0 && int.TryParse(args[0], out length))
            {
                if (length > MAX_TIME) {
                    length = MAX_TIME;
                }
            }
            
            System.Threading.Thread.Sleep(2000);

            ScreenRecorder.Record(length * 30); // seconds * 30 fps
            //ScreenRecorder.CreateFiles();
        }
    }
}